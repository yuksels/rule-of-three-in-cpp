#include <string>
#include <iostream>
class Test {
public:
    std::string str;
    Test() {}
};

class Deneme {
public:
    int a;
    Test * tst;
    Deneme() {
        std::cout<<"called def const"<<std::endl;
    }
    Deneme(const Deneme& t) {
        std::cout<<"called copy cons."<<std::endl;
        a = t.a ;
        tst = new Test;

        tst->str = t.tst->str;
    }
    ~Deneme() {
        std::cout<<"called destr."<<std::endl;
        delete  tst;
    }
    Deneme &operator=(const Deneme& t){
        a = t.a ;
        std::cout<<"called copy assi"<<std::endl;

        tst = new Test;
        tst->str = t.tst->str;
        return *this;
    }

};

int main()
{
    Deneme d;
    d.tst = new Test;
    d.tst->str = "asdas";
    auto a = d;
    a =d;
    return 0;
}
